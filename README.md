# rRootage

This is a shoot 'em up game. It's originally written by Kenta Cho.

This is a modification of the original game available
[here](http://www.asahi-net.or.jp/~cs8k-cyu/windows/rr_e.html).


## How to build

This game uses [Meson](http://mesonbuild.com/) which requires
[Ninja](https://ninja-build.org/) and [Python](https://www.python.org/). You
also need a C/C++ compiler with the following libraries:

- [GLU](https://cgit.freedesktop.org/mesa/glu/)
- [SDL 2.0](https://www.libsdl.org/)
- [SDL_mixer 2.0](https://www.libsdl.org/projects/SDL_mixer/)

If you want to run the game from this folder:

    $ meson build --buildtype=release -Dlocaldata=true
    $ ninja -v -C build
    $ build/src/rrootage

or if you want to package this:

    $ meson build --buildtype=plain --prefix=$prefix
    $ ninja -v -C build
    $ DESTDIR=$pkgdir ninja -v -C build install


## How to play

|Function|Keyboard|Joystick|
|---|---|---|
|Movement|Arrow keys|Joystick|
|Laser|Z|Button 1/4|
|Special|X|Button 2/3|
|Pause|P||

Select the stage by a keyboard or a joystick.
Press a laser key to start the game.

You can also select the game mode from 4 types.
Press a special key to change the game mode.

- **Normal mode**

  This is the standard game mode.
  Your ship becomes slow while holding the laser key.

  *Special:* Bomb

  The bomb wipes enemies' bullets.
  The number of bombs are displayed at the right-down corner.

- **PSY mode**

  As your ship grazes a bullet,
  the graze meter (displayed at the right-down corner) increases.
  When the graze meter becomes full, the ship becomes invincible for a while.

  *Special:* Rolling

  This movement widen the range that the ship can graze.
  While holding this key, the ship becomes slow.
  If you want to move faster, tap this key.

- **IKA mode**

  Your ship has two attributes, white and black.
  All bullets also have these attributes,
  and your ship can absorb bullets that has the same attribute.
  Absorbed bullets are changed into lasers automatically.

  *Special:* Attribution change

  Change your ships attribute.

- **GW mode**

  Your ship can use the reflector.
  The reflector reflects bullets around your ship.

  *Special:* Reflector

  To use the reflector, you have to hold this key until
  the reflector meter (displayed at the right-down corner) becomes empty.
  You can use the reflector only if the reflector meter displays *OK*.

Control your ship and avoid the barrage.
Use the laser to destroy the battleship of the enemy.
You can cause more damage if you fire the laser close to the enemy.

When all ships are destroyed, the game is over.
The ship extends 200,000 and every 500,000 points.


## Command line options

    --nosound       Disable sound
    --window        Launch the game in window mode
    --reverse       Reverse the laser key and the bomb key


## Screenshots

![](shot0.png)
![](shot1.png)
![](shot2.png)
![](shot3.png)


---

rRootage is © Kenta Cho under BSD

Modifications are © Christian Buschau
